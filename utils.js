export const graphQlUrlize = (query) => {
  if (query.hasOwnProperty('query')) {
    let params = 'query=' + encodeURIComponent(query.query)
    params += query.hasOwnProperty('variables') ? '&variables=' + encodeURIComponent(JSON.stringify(query.variables)) : ''

    return '?' + params
  }
  return ''
}

// Декодирует курсор GraphQL Edge
export const decodeGraphQLCursor = cursor => {
  return parseInt(Buffer.from(cursor, 'base64').toString().split(':')[1])
};

// Кодирует курсор GraphQL Edge
export const encodeGraphQLCursor = (cursor, context = 'arrayconnection') => {
  return Buffer.from(`${context}:${cursor}`).toString('base64')
};
